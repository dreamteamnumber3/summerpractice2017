#ifndef FUNCTION_H
#define FUNCTION_H


void GetMatrix(double**, int, int);
void PrintMatrix(double**, int, int);
double CalculateDeterminantTwoByTwo(double**, int type = 1);
double CalculateDeterminantThreeByThree(double**, int type = 1);
double CalculateDeterminant(double**, int);
void GetCoefficientsofSquareEquation(double*, double*, double*);
double SolveSquareEquationForX(double, double, double);
double SolveBiquadraticEquationForX(double, double, double);
double** TransposeMatrix(double**, int, int);
double MultiplyMatrices(double**, int, int, double**, int, int);
double DegreesIntoRadians(int);
void BradisTableQuery(double, int NewMinutes = 0);
double **GetInvertibleMatrix(double **, int);
void GetCoefficientsofCubicEquation(double*, double*, double*, double*);
void SolveCubicEquationForX(double, double, double, double);
double TakeCubicRadical(double);

#endif