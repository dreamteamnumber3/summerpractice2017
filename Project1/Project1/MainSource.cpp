#include "AdvancedCalculator.h"
#include <iostream>
#include <conio.h>
#include <locale.h>

using namespace std;

int Menu();
int MenuMath();
int MenuMatrix();
int MenuBachgroundMaterials();

const double Pi = 3.1415926535897932384626433832795;

int main()
{
	setlocale(LC_ALL, "rus");
	while (1)
	{
		system("cls");
		switch (Menu())
		{
		case 1:
		{
			system("cls");
			switch (MenuMath())
			{
			case 1:
			{
				system("cls");
				double A = 1, B = 1, C = 1;
				cout << "����� ��� ����������� ���������: \n" << "A*x^2 + B*x + C = 0\n"
					"���� � ��������� ����������� x^2 �(���) x, ������, ����������� ��� ��� ����� 0\n";
				cout << "������� ������������. \n";
				GetCoefficientsofSquareEquation(&A, &B, &C);
				SolveSquareEquationForX(A, B, C);
				break;
			}
			case 2:
			{
				system("cls");
				double A = 1, B = 1, C = 1;
				cout << "����� ��� ������������� ���������: \n" << "A*x^4 + B*x^2 + C = 0\n"
					"���� � ��������� ����������� x^4 �(���) x^2, ������, ����������� ��� ��� ����� 0\n";
				cout << "������� ������������. \n";
				GetCoefficientsofSquareEquation(&A, &B, &C);
				SolveBiquadraticEquationForX(A, B, C);
				break;
			}
			case 3:
			{
				system("cls");
				double A = 1, B = 1, C = 1, D = 1;
				cout << "����� ��� ����������� ���������: \n" << "A*x^3 + B*x^2 + C*x + D = 0\n"
					"���� � ��������� ����������� x^3, x^2 �(���) x, ������, ����������� ��� ��� ����� 0\n";
				cout << "������� ������������. \n";
				GetCoefficientsofCubicEquation(&A, &B, &C, &D);
				SolveCubicEquationForX(A, B, C, D);
				break;
			}
			case 4:
			{
				system("cls");

				break;
			}
			default:
			{
				cout << endl << "������������ �������" << endl;
				_getch();
			}
			}
			break;
		}
		case 2:
		{
			system("cls");
			switch (MenuMatrix())
			{
			case 1:
			{
				system("cls");
				int MSize;
				cout << "������� ������ ������� NxN: ";
				cin >> MSize;
				double detM;
				double **NewMatrix = new double *[MSize];
				for (int i = 0; i < MSize; i++)
					NewMatrix[i] = new double[MSize];
				GetMatrix(NewMatrix, MSize, MSize);
				if (MSize < 2)
				{
					cout << "������! ����������� ������� �� ����� ��������� ���� ��� ���� �������������.\n"
						"��� ������� ������������ 1x1 �������� � ������������� �������� ����� �������� �������������.";
					_getch();
				}
				if (MSize == 2)
					CalculateDeterminantTwoByTwo(NewMatrix);
				if (MSize == 3)
					CalculateDeterminantThreeByThree(NewMatrix);
				if (MSize > 3)
				{
					char answer[4];
					detM = CalculateDeterminant(NewMatrix, MSize);
					cout << "������������ ��������� ������� ����� " << detM << ".";
					cout << "���� �� ������ ������� ������� ��������� � ���������� ������������ ������� �������, �������� ��� 3, ������� <1>.\n"
						"��� ������ ������� ����� ������ �������.\n";
					cout << "������� ��� �����: ";
					cin >> answer;
					if ((strcmp(answer, "1") == 0))
					{
						cout << "����� ��������� ������������, ������������� ����������� �� ������ ������. \n"
							"��� ����� ����� ������ �� ��������� ������ ������, �������� ����������� ������ � �������, � ������� �� ���������, \n"
							"� �� ���������� ��������� ������� ���������� ����� �������, ����� ���� ������� �� ������������.\n"
							"����� ����� ���������� ���� ����� ���������. ��� ����� ������ ������ ������ � ������� ��������. \n"
							"���� ����� ������, �� ���� �������������, � ���� �������� - �������������. \n"
							"��������� ����� ����� �������� ��������� �������� � ������ ����� �� ������������ ����� �������. \n"
							"��� ��������� ��� ������� �������� �� ������ ������ � �������. \n"
							"���� ������� ������ 4-�� �������, �� ������ �������� ���������� ��������� �� ��� ���, ���� ����� ������� ��� ������������ �� ������ 3-�� �������. \n" ;
					}
					_getch();
				}
				delete[] NewMatrix;
				break;
			}
			case 2:
			{
				system("cls");
				int N, M;
				cout << "������� ������� �������.\nM-���������� ��������: ";
				cin >> M;
				cout << "N-���������� �����: ";
				cin >> N;
				double **NewMatrix = new double *[N];
				for (int i = 0; i < N; i++)
					NewMatrix[i] = new double[M];
				GetMatrix(NewMatrix, N, M);
				if (M < 2 || N < 2)
				{
					cout << "������! ����������� ������� �� ����� ��������� ���� ��� ���� �������������.\n"
						"��� ������� ������������ 1x1 ���������������� ������ �� ��������.";
					_getch();
				}
				cout << "��� ���� ����� ��������������� �������, ���������� ������ �������� �\n"
					"�������, � ������� � ������.\n";
				PrintMatrix(TransposeMatrix(NewMatrix, N, M), M, N);
				for (int i = 0; i < N; i++)
				{
					delete [] NewMatrix[i];
				}
				break;
			}
			case 3:
			{
				system("cls");
				int MSize;
				cout << "������� ������ �������, ��� ������� �� ������ ����� ��������.\n"
					<< "����������: �������� ������� ����� ����� � ������ � ��� ������, ���� �������� ������� ����������, ������� �������� ������ ���� ��������.\n"
					<< "������ �������� �������: ";
				cin >> MSize;
				while (MSize < 1)
				{
					cout << "������! ����������� ������� �� ����� ���� ������������� ��� ������ ����. ���������� ��� ���.\n"
						<< "������ �������� �������: ";
					cin >> MSize;
				}
				double **NewMatrix = new double*[MSize];
				for (int i = 0; i < MSize; i++)
					NewMatrix[i] = new double[MSize];
				GetMatrix(NewMatrix, MSize, MSize);
				double ** ResultMatrix = new double*[MSize];
				for (int i = 0; i < MSize; i++)
					ResultMatrix[i] = new double[MSize];
				ResultMatrix = GetInvertibleMatrix(NewMatrix, MSize);
				if (ResultMatrix != NewMatrix)
				{
					cout.precision(2);
					PrintMatrix(ResultMatrix, MSize, MSize);
					delete[]ResultMatrix;
					cout << "���� �� ������ ������� ������� ��������� � ���������� �������� �������, ������� <1>.\n"
						"��� ������ ������� ����� ������ �������.\n";
					cout << "������� ��� �����: ";
					char answer[4];
					cin >> answer;
					if ((strcmp(answer, "1") == 0))
					{
						cout << "1. ������� ������������ �������� �������:\n"
							"   a) ���� ������������ �������� ������� ����� ����� 0, �� ���������� �������� �� ������� ����������.\n"
							"   b) ���� ������������ �������� ������� �� ����� 0, �� ����� ���������� ���������� �������� �������.\n"
							"2. ����� �������� ������� �������������� ����������:\n"
							"   a) ����� �� ������� ������ �� ��������� ������� � �������� ����������� ������ � �������, � ������� �� ���������.\n"
							"   b) �� ���������� ��������� ������ ����� ������� � ������� �� �����������.\n"
							"   �) ����� ���, ��� �������� ���������� ����� � ������� �������������� ����������, ����� ���������� ��� ����, ��� ���� ������ ������ ������ � �������, ������� �� ����������:\n"
							"         - ���� ����� ������, �� ���� <+>."
							"         - ���� ����� ��������, �� ���� <->."
							"   d) ���������� �����, ���������� � ������ b), � ������ ����� �� ����������� ������ � ������� �������������� ���������� �� �� �������, �� ������� ��������� ��������� � ������ �) ������� � �������� �������.\n"
							"3. ������������� ������� �������������� ����������, �.�. ������ ������� ������ � �������.\n"
							"4. �������� ����������������� ������� �� 1, �������� �� ����������� ����� ������������ �������� �������, � ����� ������� �������� ��������.\n";
					}
					_getch();
				}
				delete[]NewMatrix;
				break;
			}
			case 4:
			{
				system("cls");
				int MheightA, MwidthA, MheightB, MwidthB;
				cout << "������� ���������� ����� � �������� ������ ������� ����� ������: ";
				cin >> MheightA >> MwidthA;
				double **NewMatrixA = new double *[MheightA];
				for (int i = 0; i < MheightA; i++)
					NewMatrixA[i] = new double[MwidthA];
				GetMatrix(NewMatrixA, MheightA, MwidthA);
				cout << "������� ���������� ����� � �������� ������ ������� ����� ������: ";
				cin >> MheightB >> MwidthB;
				double **NewMatrixB = new double *[MheightB];
				for (int i = 0; i < MheightB; i++)
					NewMatrixB[i] = new double[MwidthB];
				GetMatrix(NewMatrixB, MheightB, MwidthB);
				MultiplyMatrices(NewMatrixA, MheightA, MwidthA, NewMatrixB, MheightB, MwidthB);
				char answer;
				cout << "\n����� �������� ������� ������������� �������, ������� 1. ��� ������ �� ������� ������� ����� ������ ������: ";
				cin >> answer;
				if (answer == '0')
				{
					cout << "����������� ������������ ���� ������ � � � �������� ����� ������� �, � ������� �������, ������������� "
						"i-�� ������ � j-�� �������, ����������� ��� ������������ i-�� ������ ������ ������� � �� j-�� ������� ������ ������� �. "
						"� ����� ������� ���������� ����� �����, ��� � ������ �������, � ���������� �������� �������� � ����������� �������� ������ �������.";
					_getch();
				}
				break;
			}
			case 5:
			{
				system("cls");
				int MSize, answer, N, M;
				cout << "���������� ��������� ���������, ��� X - ����������� �������, ����� ���� ������������ � ���� �����.\n"
					"1. A * X = B\n"
					"2. X * A = B\n";
				cout << "������� 1 ��� 2 ����� ������� ��� ������ ���������: "; cin >> answer;

				cout << "������� ������ ������� A.\n"
					"����������: �������� ������� ����� ����� � ������ � ��� ������, ���� �������� ������� ����������, ������� �������� ������ ���� ��������.\n"
					"������ �������� �������: ";
				cin >> MSize;
				while (MSize < 1)
				{
					cout << "������! ����������� ������� �� ����� ���� ������������� ��� ������ ����. ���������� ��� ���.\n"
						<< "������ �������� �������: ";
					cin >> MSize;
				}
				double **NewMatrix = new double*[MSize];
				for (int i = 0; i < MSize; i++)
					NewMatrix[i] = new double[MSize];
				GetMatrix(NewMatrix, MSize, MSize);
				double ** ResultMatrix = new double*[MSize];
				for (int i = 0; i < MSize; i++)
					ResultMatrix[i] = new double[MSize];
				cout << "������� ������� ������� B.\nM - ���������� ��������: ";
				cin >> M;
				cout << "N - ���������� �����: ";
				cin >> N;
				double **NewMatrix1 = new double *[N];
				for (int i = 0; i < N; i++)
					NewMatrix1[i] = new double[M];
				GetMatrix(NewMatrix1, N, M);

				cout << "\n������� �������� ������� ��� ������� A.\n";
				ResultMatrix = GetInvertibleMatrix(NewMatrix, MSize);
				PrintMatrix(ResultMatrix, MSize, MSize);
				if (answer == 1)
				{
					cout << "�� ��������� ������ �������:\n A * X = B  =>  X = A^(-1) * B,\n��� �^(-1) - �������� �������.\n"
						"�������� ��������� ������ � ������ �������.\n";
					MultiplyMatrices(ResultMatrix, MSize, MSize, NewMatrix1, N, M);

				}
				if (answer == 2)
				{
					cout << "�� ��������� ������ �������:\n A * X = B  =>  X = B * A^(-1),\n��� �^(-1) - �������� �������.\n"
						"�������� ��������� ������ � ������ �������.\n";
					MultiplyMatrices(NewMatrix1, N, M, ResultMatrix, MSize, MSize);
				}
				system("pause");
				delete[] NewMatrix;
				delete[] NewMatrix1;
				delete[] ResultMatrix;
				break;
			}
			case 6:
			{
				system("cls");

				break;
			}
			default:
			{
				cout << endl << "������������ �������" << endl;
				_getch();
			}
			}
			break;
		}
		case 3:
		{
			system("cls");
			switch (MenuBachgroundMaterials())
			{
			case 1:
			{
				system("cls");
				double Angle;
				int Minutes;
				char Answer[2];
				cout << "����� ������ �������� ����, �������:\n"
					"1 - � ��������;\n"
					"2 - � ��������.\n";
				cin >> Answer;
				if ((strcmp(Answer, "1") == 0))
				{
					cout << "��� ����� ���������� �������� ���� ������� ������ ����������� ��� pi.\n"
						"��������, ��� pi/2 - 0.5\n"
						"���� fi = ";
					cin >> Angle;
					Angle *= Pi;
					BradisTableQuery(Angle);
				}
				else
				{
					cout << "���� � �������� ����� ���� ����� � ���� fi = X �������� Y �����.\n"
						"������ ������ ������������ ��� 18'. \n"
						"���� � ����� ������ ���������� ����� �� �������, ������� 0 ������ �� ����������. \n"
						"��������! ������ ��������� ���������� ������������ ���������� ����� �� �������� ��� �������� ����, � �� ��������� ����� ������ ��������.\n";
					cout << "���� fi = ";
					cin >> Angle;
					Angle = DegreesIntoRadians(Angle);
					cout << "���������� ����� = ";
					cin >> Minutes;
					BradisTableQuery(Angle, Minutes);
				}
				break;
			}
			case 2:
			{
				system("cls");

				break;
			}
			default:
			{
				cout << endl << "������������ �������" << endl;
				_getch();
			}
			}
			break;
		}
		case 4:
		{
			return 0;
		}
		default:
		{
			cout << endl << "������������ �������" << endl;
			_getch();
		}
		}
	}
}

int Menu()
{
	int er;
	cout << "����:\n";
	cout << "1 - �������������� ���������;\n";
	cout << "2 - �������� ��� ���������;\n";
	cout << "3 - ���������� ���������;\n";
	cout << "4 - �����.\n";
	cin >> er;
	return er;
}

int MenuMath()
{
	int er;
	cout << "������� ��� �����������: \n";
	cout << "1 - ������� ���������� ���������;\n";
	cout << "2 - ������� ������������ ���������;\n";
	cout << "3 - ������� ���������� ���������;\n";
	cout << "4 - �����.\n";
	cin >> er;
	return er;
}

int MenuBachgroundMaterials()
{
	int er;
	cout << "������� ��� �����������: \n";
	cout << "1 - ������ �� ���������� ������� �������;\n";
	cout << "2 - �����.\n";
	cin >> er;
	return er;
}

int MenuMatrix()
{
	int er;
	cout << "������� ��� �����������: \n";
	cout << "1 - ����� ������������ ���������� �������;\n";
	cout << "2 - ���������������� ������;\n";
	cout << "3 - ����� �������� �������;\n";
	cout << "4 - ���������� ������������ ������;\n";
	cout << "5 - ������� ���������� ��������� ���������.\n";
	cout << "6 - �����.\n";
	cin >> er;
	return er;
}